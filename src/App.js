import React from 'react'
import Header from "./components/header/header"
import {GlobalStyle} from "./style";
import Hero from "./components/hero/hero";
import PrefooterBanner from "./components/prefooter-banner/prefooter-banner";
import ContentFullPage from "./components/layout/ContentFullPage";
import CtaBanner from "./components/cta-banner/cta-banner";
import Footer from "./components/footer/footer";

function App() {
  return (
  
      <ContentFullPage>
        <GlobalStyle/>
        <Header/>
        <Hero/>
          <CtaBanner/>
          <PrefooterBanner/>
          <Footer/>
      </ContentFullPage>
  )
}

export default App
