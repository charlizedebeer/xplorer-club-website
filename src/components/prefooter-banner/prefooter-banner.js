import React from "react";
import styled from "styled-components";
import PfIcon from "./icon.png"

const Banner = styled.section `
  background: #F2F2F2;
  width: 100vw;`
const Content = styled.div `
  padding: 15vh 10vw;
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;`

const Heading = styled.text `
  font-size: calc(1rem + 2vw);
  font-style: normal;
  font-weight: 700;
  line-height: calc(1rem + 2.5vw);
  text-align: center;
`

const Text = styled.text `
  font-size: calc(1rem + 1vw);;
  padding: 2vh 5vw 5vh 5vw;
  font-style: normal;
  font-weight: 400;
  line-height:calc(1rem + 1.5vw);
  text-align: center;
  @media screen and (max-width: 700px){
    padding: 2vh 2vw 5vh 2vw;
    font-size: calc(1rem + 1.5vw);;

  }
  @media screen and (max-width: 450px){
    padding: 2vh 1vw;
    font-size: calc(0.76rem + 1.5vw);;


  }
  
  

`

const BannerIcon = styled.img `
width: calc(5rem + 1vw);
  align-self: center;
  margin-bottom: 5vh;
  
`


const PrefooterBanner = () => {
    return(
        <Banner>
            <Content>
                <BannerIcon src={PfIcon}/>
                <Heading>
                    Bonuses & Beneﬁts
                </Heading>
                <Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh erat, tempus
                    vestibulum sapien eu, maximus tincidunt e
                </Text>
            </Content>
        </Banner>
    )
}

export default PrefooterBanner