import React from 'react'
import styled from "styled-components";
import BgImg from "./hero-bg.webp";
import CardImg from './xplorer-card.webp'
import TickedBox from './ticked.png'
import UntickedBox from './unticked.png'
import TickedBoxLrg from './ticked-lrg.png'
import UntickedBoxLrg from './unticked-lrg.png'
import GoldCard from "./xpc-premium-gold.png"
import SilverCard from "./xpc-standard-silver.png"


const HeroContainer = styled.div`
  width: 100%;
  display: grid;
`
const HeroMain = styled.div`
  background: url(${BgImg}) top center no-repeat;
  background-origin: content-box;
  background-size: cover;
  min-height: calc(20vw + 20vh);
  min-width: 100vw;
  @media screen and (max-width: 600px) {
    background-size: cover ;
    background-position: top right;
    min-height: fit-content;

  }
  @media screen and (max-width: 500px) {
    background-size: cover ;
    margin-top: 15%;
    background-position: right 46% bottom;
    min-height: fit-content;

  }
  @media screen and (max-width: 300px) {
    background-size: cover ;
    margin-top: 20%;
    background-position: right 36% bottom;
    min-height: fit-content;

  }

`
const Text = styled.div`
  text-overflow: fade;
  max-width: 40%;
  word-wrap: normal;
  word-break: normal;
  font-size: calc(1rem + 1vw);
  font-style: normal;
  font-weight: normal;
  line-height: calc(1.5rem + 1vw);
  padding-bottom: 5vw;
  text-align: left;
  margin: calc(10vw + 5vh) 0 calc(5vw + 5vh) 10vw;
  @media screen and (max-width: 930px) {
    max-width: calc(55% + 1vw );
  }
  @media screen and (max-width: 500px) {
    max-width: 70% ;
    margin-top: 15vw;
    margin-bottom: 10vw;
    padding-bottom: 0;

  }
  @media screen and (max-width: 300px) {
    max-width: 80% ;
    padding-bottom: 0;

  }



`
const Bold = styled.text`
font-weight: bold;
`



const XplorerCard = styled.img`
  width:55vw;
  margin-top: -10vw;
justify-self: center;
  @media screen and (max-width: 700px) {
    display: none;
  }

`
const XplorerCardMobile = styled.img`
  width:250px;
  display: inline-flex;
justify-self: center;
  margin-bottom: 2rem;
  grid-area: CardImg;
  @media screen and (min-width: 700px) {
    display: none;
  }
`

const CardOptions = styled.div`
margin: 10vh calc(1rem + 1vw);
    display: grid;
  align-items: center;
  column-gap: 4vw;
  grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
  grid-template-rows: 1fr;
    grid-auto-flow: row;
    grid-template-areas:
    "StandardCard PremiumCard";
  @media screen and (max-width: 700px){
    grid-template-columns: 1fr ;
    grid-template-rows: 1fr 1fr;
    grid-auto-flow: column;
    row-gap: 10vh;
    grid-template-areas:
    "StandardCard" 
    "PremiumCard" ;

  }
`
const StandardCard = styled.div`
grid-area: StandardCard;
  width: 100%;
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: repeat(auto-fit 3 1fr);
  align-content: center;
  grid-template-areas: 
    "Heading"
    "CardFeatureItems"
    "Button";
  @media screen and (max-width: 700px){
    align-content: center;
    justify-content: center;
    grid-template-rows: repeat(auto-fit 4 1fr);
    grid-template-areas:
     "CardImg"
     "Heading"
     "CardFeatureItems"
     "Button";

  }
`
const PremiumCard = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: repeat(auto-fit 3 1fr);
  align-content: center;
  grid-template-areas: 
  
    "Heading"
    "CardFeatureItems"
    "Button";
  @media screen and (max-width: 700px){
    align-content: center;
    justify-content: center;
    grid-template-rows: repeat(auto-fit 4 1fr);
    grid-template-areas:
     "CardImg"
     "Heading"
     "CardFeatureItems"
     "Button";

  }
`

const Features = styled.ul `
display: grid;
  justify-content: center;

  font-size: calc(1rem + 1vw);
  grid-area: CardFeatureItems;
  @media screen and (max-width: 950px){
    font-size: calc(0.8rem + 1vw) ;
  }


`
const IncludedItem = styled.li`
  list-style:  url(${TickedBoxLrg}) outside middle;
  padding: 0.5vw calc(1rem + 1vw);
  @media screen and (max-width: 550px){
    list-style:  url(${TickedBox}) outside middle;
    font-size: calc(1rem + 1vw);
    padding: 1vw calc(1rem + 1vw);


  }
`;
const ExcludedItem = styled.li`
  list-style:  url(${UntickedBoxLrg}) outside middle;
  padding: 0.5vw calc(0.5rem + 1vw);
  @media screen and (max-width: 550px){
    list-style:  url(${UntickedBox}) outside middle;
    font-size: calc(1rem + 1vw);
    padding: 1vw calc(1rem + 1vw);


  }`

const Heading = styled.div `
  font-weight: bold;
  grid-area: Heading;
  font-size: calc(1rem + 1vw);
  line-height: calc(1rem + 1vw);
  justify-self: center;
  @media screen and (max-width: 950px){
    font-size: calc(0.75rem + 1vw) ;

  }
  @media screen and (max-width: 700px){
    font-size: calc(1rem + 1vw) ;

  }
  @media screen and (max-width: 350px){
    font-size: calc(0.95rem + 1vw) ;
    text-align: center;

  }
`
const DarkButton = styled.button `
  justify-self: center;
background: #666666;
  color: white;
  width:  calc(30vw + 1vw);
  padding: 1.25rem calc(1.5rem + 1vw);
  margin: 1rem;
  border-radius: 5rem;
  font-size: calc(0.25rem + 1vw) ;
  font-weight: bold;
  border: none;
  grid-area: Button;

  @media screen and (max-width: 950px){
    width:  fit-content;
    padding: 1rem 0;
    min-width: 40vw;
    font-size: calc(0.5rem + 1vw) ;

  }
  @media screen and (max-width: 700px){
    width:  fit-content;
    padding: 1rem 6vw;
    min-width: 40vw;
    font-size: calc(0.5rem + 1vw) ;

  }
  @media screen and (max-width: 250px){
    width:  fit-content;
    min-width: 80vw;
    padding: 1rem 0;

    font-size: calc(0.5rem + 1vw) ;

  }
  
`
const GoldButton = styled.button `
    background: linear-gradient(90.82deg, #9C6D31 6.91%, #CD9B48 24.19%, #F6C76F 40.39%, #F6DC7F 52.81%, #9C6D31 67.39%, #CD9B48 80.89%, #F6C76F 97.63%, #F6DC7F 110.05%);
    color: black;
  width: fit-content;
  justify-self: center;
  min-width:  calc(30vw + 1vw );
  padding: 1.25rem calc(2rem + 1vw);
  margin: 1rem;
  border-radius: 5rem;
  font-size: calc(0.25rem + 1vw) ;
  font-weight: bold;
  border: none;
  grid-area: Button;
  @media screen and (max-width: 950px){
    width:  fit-content;
    padding: 1rem 0;
    min-width: 40vw;
    font-size: calc(0.5rem + 1vw) ;

  }
  @media screen and (max-width: 700px){
    width:  fit-content;
    padding: 1rem 6vw;
    min-width: 40vw;
    font-size: calc(0.5rem + 1vw) ;

  }
  @media screen and (max-width: 250px){
    width:  fit-content;
    min-width: 80vw;
    padding: 1rem 0;

    font-size: calc(0.5rem + 1vw) ;

  }

`

const Hero = () => {
    return (
<HeroContainer>
<HeroMain>

    <Text>
      When you join the <Bold>Xplorer Club</Bold> you will,
        besides the personal offer, receive a travel  card that is connected to MasterCard.
    </Text>

</HeroMain>
    <XplorerCard src={CardImg}/>


    <CardOptions>
        <StandardCard>
            <XplorerCardMobile src={SilverCard}/>
            <Heading>
                    Standard Xplorer Club for 99 USD
                </Heading>
                <Features>
                    <IncludedItem>Xplorer Card</IncludedItem>
                    <IncludedItem> Travel insurance</IncludedItem>
                    <IncludedItem>Hotel & car rental offers</IncludedItem>
                    <ExcludedItem> Priority pass for flights</ExcludedItem>
                    <ExcludedItem>Access to airport lounges</ExcludedItem>
                </Features>
                <DarkButton>
                    Apply for Standard Xplorer Card
                </DarkButton>
        </StandardCard>

        <PremiumCard>
            <XplorerCardMobile src={GoldCard}/>
            <Heading>
                    Premium Xplorer Club for 149 USD
                </Heading>
                <Features>

                    <IncludedItem>Xplorer Card</IncludedItem>
                    <IncludedItem> Travel insurance</IncludedItem>
                    <IncludedItem>Hotel & car rental offers</IncludedItem>
                    <IncludedItem> Priority pass for flights</IncludedItem>
                    <IncludedItem>Access to airport lounges</IncludedItem>

                </Features>
                <GoldButton >
                    Apply for Premium Xplorer Card
                </GoldButton>
        </PremiumCard>
    </CardOptions>
</HeroContainer>
    )
}

export default Hero
